Name:           fastclick
Version:        0.2.0
Release:        1%{?dist}
Summary:        Fastclick modular router with xdp extensions

License:        MIT
URL:            https://gitlab.com/mergetb/tech/fastclick

BuildRequires:  libbpf-devel gcc-c++ make clang llvm perl-interpreter perl-File-Which
Requires:       libbpf

%description
Fastclick modular router with XDP extensions


%build
CXXFLAGS=-Wno-pointer-arith
%{_sourcedir}/configure --prefix=/usr --enable-xdp --enable-multithread --disable-linuxmodule --enable-intel-cpu --disable-batch --enable-user-multithread --disable-dynamic-linking --enable-poll --enable-bound-port-transfer --disable-dpdk --with-netmap=no --enable-zerocopy --disable-dpdk-packet --enable-local --enable-etherswitch
%make_build


%install
rm -rf $RPM_BUILD_ROOT
/usr/bin/make install DESTDIR=%{buildroot} INSTALL="/usr/bin/install"


%files
/usr/bin/*
/usr/include/click/*
/usr/include/clicknet/*
/usr/include/clicktool/*
#/usr/local/lib/click/*
/usr/lib/*
/usr/share/click/*
/usr/share/man/man*/*
#/usr/lib/debug/*



%changelog
* Fri Nov 19 2021 Michael Elkins <elkins@isi.edu>
- 
